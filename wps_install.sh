#/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "ОШИБКА: нужны права root!"
  exit
fi

echo "Установка WPS-office"
emerge wps-office --autounmask-write
etc-update
emerge wps-office