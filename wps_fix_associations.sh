#/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "ОШИБКА: нужны права root!"
  exit
fi

rm /usr/share/mime/packages/wps-office*
update-mime-database /usr/share/mime