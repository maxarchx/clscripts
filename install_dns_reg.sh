#/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "ОШИБКА: нужны права root!"
  exit
fi
echo "Настроим обновление DNS"
echo "Настроим обновление DNS: script"
echo "#/bin/sh
net ads dns register -Udnsregister%Dnsregister12345" > /etc/update_dns_reg.sh
chown root:root /etc/update_dns_reg.sh
chmod u+x /etc/update_dns_reg.sh
chmod og-rw /etc/update_dns_reg.sh
echo "Настроим обновление DNS: cron"
crontab -l | { cat; echo "*/10 * * * * /etc/update_dns_reg.sh > /dev/null 2>&1"; } | crontab -