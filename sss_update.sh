#/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "ОШИБКА: нужны права root!"
  exit
fi
/etc/init.d/sssd restart

echo "Настроим автоподмену доменных групп"
sss_override group-add linuxlocalvideo -g 27
sss_override group-add linuxlocalplugdev -g 440
sss_override group-add linuxlocalusers -g 100
sss_override group-add linuxlocalaudio -g 18
sss_override group-add linuxlocalcdrom -g 19
sss_override group-add linuxlocalfloppy -g 11
sss_override group-add linuxlocalcdrw -g 80
sss_override group-add linuxlocalscanner -g 441
sss_override group-add linuxlocalusb -g 85
sss_override group-add linuxlocalsudo -g 443

echo "Запустим sssd"
/etc/init.d/sssd restart
